find -name '*.h' -exec sed -i 's/( /(/g' {} +
find -name '*.hpp' -exec sed -i 's/( /(/g' {} +
find -name '*.cpp' -exec sed -i 's/( /(/g' {} +

find -name '*.h' -exec sed -i 's/ )/)/g' {} +
find -name '*.hpp' -exec sed -i 's/ )/)/g' {} +
find -name '*.cpp' -exec sed -i 's/ )/)/g' {} +

find -name '*.h' -exec sed -i 's/< \(.*\) >/<\1>/g' {} +
find -name '*.hpp' -exec sed -i 's/< \(.*\) >/<\1>/g' {} +
find -name '*.cpp' -exec sed -i 's/< \(.*\) >/<\1>/g' {} +

find -name '*.h' -exec sed -i 's/\[ /\[/g' {} +
find -name '*.hpp' -exec sed -i 's/\[ /\[/g' {} +
find -name '*.cpp' -exec sed -i 's/\[ /\[/g' {} +

find -name '*.h' -exec sed -i 's/ \]/\]/g' {} +
find -name '*.hpp' -exec sed -i 's/ \]/\]/g' {} +
find -name '*.cpp' -exec sed -i 's/ \]/\]/g' {} +
