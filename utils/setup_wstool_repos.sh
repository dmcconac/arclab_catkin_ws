cd ../src
wstool init &> /dev/null

wstool set arc_utilities                    --git git@github.com:WPI-ARC/arc_utilities.git
wstool set deform_control                   --git git@github.com:WPI-ARC/deform_control.git
wstool set smmap                            --git git@bitbucket.org:dmcconac/smmap.git
wstool set KinematicsToolbox                --git git@github.com:WPI-ARC/KinematicsToolbox.git

#wstool set bullet3                          --git git@github.com:WPI-ARC/bullet3.git
#wstool set ros_bullet                       --git git@bitbucket.org:dmcconac/ros_bullet.git
#wstool set deformable_learning_simulators   --git git@github.com:WPI-ARC/deformable_learning_simulators.git
#wstool set deformable_planners              --git git@github.com:WPI-ARC/deformable_planners.git
#wstool set sdf_tools                        --git git@github.com:WPI-ARC/sdf_tools.git
#wstool set xtf                              --git git@github.com:WPI-ARC/xtf.git
    
