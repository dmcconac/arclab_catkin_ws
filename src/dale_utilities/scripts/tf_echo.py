#!/usr/bin/env python

import rospy
import tf2_ros
import time
import sys
import IPython


def main(argv):

    if len(argv) != 3 and len(argv) != 4:
        print '    Usage: tf_echo.py <parent_frame_id> <child_frame_id> rate'
        return

    rospy.init_node('tf_echo_verbose')

    parent = argv[1]
    child = argv[2]
    if len(argv) == 3:
        rate = rospy.Rate(1.0)
    else:
        rate = rospy.Rate(float(argv[3]))


    tf_buffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tf_buffer)
    print 'Waiting for TF to collect data'
    time.sleep(2)

    while not rospy.is_shutdown():
        print
        try:
            trans = tf_buffer.lookup_transform(parent, child, rospy.Time())
            translation = trans.transform.translation
            quaternion = trans.transform.rotation
            print trans.header
            print trans.child_frame_id
            print 'Position (xyz):    ', translation.x, ' ', translation.y, ' ', translation.z
            print 'Quaternion (xyzw): ', quaternion.x, ' ', quaternion.y, ' ' , quaternion.z, ' ' , quaternion.w
            # print trans
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            print 'No transform available'
        rate.sleep()


if __name__ == "__main__":
    main(sys.argv)