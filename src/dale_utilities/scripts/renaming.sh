#!/usr/bin/env bash

for f in *path_to_start.compressed
do
    mv "$f" "$(echo "$f" | sed s/path_to_start/rrt_path/)"
done