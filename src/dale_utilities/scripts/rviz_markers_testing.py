#!/usr/bin/env python

from IPython import embed
from copy import deepcopy
import rospy
import numpy as np
from visualization_msgs.msg import Marker, MarkerArray
from arc_utilities import transformation_helper
from geometry_msgs.msg import Point, Vector3
from std_msgs.msg import ColorRGBA


def create_axes(origin, length=0.5):
    m = Marker()
    m.header.frame_id = "world_origin"
    m.pose = transformation_helper.PoseFromMatrix(origin)
    m.type = Marker.LINE_LIST
    m.scale = Vector3(length/10.0, 0, 0)

    # X axis
    m.points.append(Point(0, 0, 0))
    m.points.append(Point(length, 0, 0))
    m.colors.append(ColorRGBA(1.0, 0.0, 0.0, 1.0))
    m.colors.append(ColorRGBA(1.0, 0.0, 0.0, 1.0))
    # Y axis
    m.points.append(Point(0, 0, 0))
    m.points.append(Point(0, length, 0))
    m.colors.append(ColorRGBA(0.0, 1.0, 0.0, 1.0))
    m.colors.append(ColorRGBA(0.0, 1.0, 0.0, 1.0))
    # Z axis
    m.points.append(Point(0, 0, 0))
    m.points.append(Point(0, 0, length))
    m.colors.append(ColorRGBA(0.0, 0.0, 1.0, 1.0))
    m.colors.append(ColorRGBA(0.0, 0.0, 1.0, 1.0))

    return m


def create_text(origin, text, scale=0.1):
    m = Marker()
    m.header.frame_id = "world_origin"
    m.pose = transformation_helper.PoseFromMatrix(origin)
    m.type = Marker.TEXT_VIEW_FACING
    m.scale = Vector3(scale, scale, scale)
    m.text = deepcopy(text)
    return m


def create_arrow(start, end, scale=(0.02, 0.06, 0)):
    m = Marker()
    m.header.frame_id = "world_origin"
    m.pose = transformation_helper.PoseFromMatrix(np.eye(4))
    m.points.append(Point(start[0], start[1], start[2]))
    m.points.append(Point(end[0], end[1], end[2]))
    m.scale = Vector3(scale[0], scale[1], scale[2])
    return m


if __name__ == '__main__':
    rospy.init_node('rviz_marker_repeater')
    vis = rospy.Publisher("visualization_marker_array", MarkerArray, queue_size=10)

    markers = MarkerArray()

    # Parent
    parent_origin = np.eye(4)
    parent = create_axes(parent_origin)
    parent.ns = "parent"
    parent.id = 1

    parent_text = create_text(parent_origin, "target\nparent")
    parent_text.ns = "parent"
    parent_text.id = 2
    parent_text.color = ColorRGBA(0, 0, 1, 1)
    parent_text.pose.position.x += 0.15
    parent_text.pose.position.y -= 0.15

    markers.markers.append(parent)
    markers.markers.append(parent_text)

    # Child
    theta = np.radians(30)
    c, s = np.cos(theta), np.sin(theta)
    child_origin = np.array([[c, -s, 0, 1.0],
                             [s,  c, 0, 0.5],
                             [0,  0, 1, 0],
                             [0,  0, 0, 1]])
    child = create_axes(child_origin)
    child.ns = "child"
    child.id = 1

    child_text = create_text(child_origin, "source\nchild")
    child_text.ns = "child"
    child_text.id = 2
    child_text.color = ColorRGBA(1, 0, 1, 1)
    child_text.pose.position.x += 0.15
    child_text.pose.position.y -= 0.15

    markers.markers.append(child)
    markers.markers.append(child_text)

    # Arrow
    transform_arrow = create_arrow(parent_origin[0:3, 3], child_origin[0:3, 3])
    transform_arrow.ns = "transform"
    transform_arrow.id = 1
    transform_arrow.color = ColorRGBA(0, 0, 0, 1)
    markers.markers.append(transform_arrow)

    # Interpretation text
    interp_origin = np.eye(4)
    interp_origin[0:3, 3] = [0.05, -0.4, 0.0]

    interp_text = create_text(interp_origin, "target", 0.05)
    interp_text.ns = "interp"
    interp_text.id = 1
    interp_text.color = parent_text.color
    markers.markers.append(interp_text)

    interp_text = create_text(interp_origin, "p = T    p")
    interp_text.ns = "interp"
    interp_text.id = 2
    interp_text.color = ColorRGBA(0, 0, 0, 1)
    interp_text.pose.position.x += 0.33
    interp_text.pose.position.y -= 0.01
    markers.markers.append(interp_text)

    interp_text = create_text(interp_origin, "source", 0.05)
    interp_text.ns = "interp"
    interp_text.id = 3
    interp_text.color = child_text.color
    interp_text.pose.position.x += 0.48
    markers.markers.append(interp_text)

    interp_text = create_text(interp_origin, "t->s", 0.05)
    interp_text.ns = "interp"
    interp_text.id = 4
    interp_text.color = ColorRGBA(0, 0, 0, 1)
    interp_text.pose.position.x += 0.36
    interp_text.pose.position.y -= 0.03
    markers.markers.append(interp_text)

    interp_text = create_text(np.eye(5), "T")
    interp_text.ns = "interp"
    interp_text.id = 5
    interp_text.color = ColorRGBA(0, 0, 0, 1)
    interp_text.pose.position.x = (parent_origin[0, 3] + child_origin[0, 3]) / 2
    interp_text.pose.position.y = (parent_origin[1, 3] + child_origin[1, 3]) / 2 + 0.05
    markers.markers.append(interp_text)

    rospy.loginfo("Spinning...")
    rate = rospy.Rate(1.0)
    while not rospy.is_shutdown():
        vis.publish(markers)
        rate.sleep()
