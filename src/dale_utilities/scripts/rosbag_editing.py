import rosbag
# import IPython

saved = {'/visualization_marker': False, '/object_estimate': False}

with rosbag.Bag('output.bag', 'w') as outbag:
    for topic, msg, t in rosbag.Bag('cloth_estimate__val_setup.bag').read_messages():
        # IPython.embed()
        if not saved[topic]:
            outbag.write(topic, msg, t)
            saved[topic] = True