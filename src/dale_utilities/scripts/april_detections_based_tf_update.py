#!/usr/bin/env python

#import rosbag
from arc_utilities import ros_helpers
from geometry_msgs.msg import PoseStamped
import tf2_geometry_msgs
import rospy
import IPython
import numpy as np
import time
from apriltag_kinect2.msg import AprilKinectDetections

#vh_bag = 'april_detections_victor_head.bag'
#tripod_bag = 'april_detections_tripodA.bag'

vh_detections = np.zeros((0, 3))
tripod_detections = np.zeros((0, 3))
# IPython.embed()


rospy.init_node('random')
tf2 = ros_helpers.TF2Wrapper()
timeout = rospy.Duration(nsecs=500 * 1000 * 1000)
time.sleep(2)


class Foo(object):
    def __init__(self):
        self.vh_detections = np.zeros((0, 4))
        self.tripod_detections = np.zeros((0, 4))

    def callback(self, msg):
        """
        :type msg: AprilKinectDetections
        :return:
        """
        for detection in msg.detections:
            pose = PoseStamped(pose=detection.pose, header=msg.header)
            pose.header.stamp = rospy.Time.now()
            world_pose = tf2.transform_to_frame(object_stamped=pose, target_frame="world_origin", timeout=timeout).pose
            if msg.header.frame_id == "kinect2_victor_head_rgb_optical_frame":
                self.vh_detections = np.vstack(
                    [self.vh_detections, [detection.id, world_pose.position.x, world_pose.position.y, world_pose.position.z]])
            else:
                self.tripod_detections = np.vstack(
                    [self.tripod_detections, [detection.id, world_pose.position.x, world_pose.position.y, world_pose.position.z]])
        pass

foo = Foo()

sub = rospy.Subscriber("/apriltag_kinect2/detections", AprilKinectDetections, foo.callback)
rospy.spin()

print "vh_detections = ["
print foo.vh_detections
print "];"

print "tripod_detections = ["
print foo.tripod_detections
print "];"

"""
print "vh_detections = ["
for topic, msg, t in rosbag.Bag(vh_bag).read_messages():
    for detection in msg.detections:
        pose = geometry_msgs.msg.PoseStamped(pose=detection.pose, header=msg.header)
        pose.header.stamp = rospy.Time.now()
        world_pose = tf2.transform_to_frame(object_stamped=pose, target_frame="world_origin", timeout=timeout).pose
        print detection.id, \
                world_pose.position.x, \
                world_pose.position.y, \
                world_pose.position.z, \
                world_pose.orientation.x, \
                world_pose.orientation.y, \
                world_pose.orientation.z, \
                world_pose.orientation.w


print "];"

print "tripod_detections = ["
for topic, msg, t in rosbag.Bag(tripod_bag).read_messages():
    for detection in msg.detections:
        pose = geometry_msgs.msg.PoseStamped(pose=detection.pose, header=msg.header)
        pose.header.stamp = rospy.Time.now()
        world_pose = tf2.transform_to_frame(object_stamped=pose, target_frame="world_origin",timeout=timeout).pose
        print detection.id, \
                world_pose.position.x, \
                world_pose.position.y, \
                world_pose.position.z, \
                world_pose.orientation.x, \
                world_pose.orientation.y, \
                world_pose.orientation.z, \
                world_pose.orientation.w
print "];"
"""
