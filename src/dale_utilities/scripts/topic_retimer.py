#! /usr/bin/env python

#################################################
#                                               #
#   Arbitrary topic retimer                     #
#                                               #
#################################################

import rospy
from copy import deepcopy


class TopicRetimer:
    def __init__(self, topic_package_type, input_topic_name, request_service):
        rospy.loginfo("Starting topic retimer...")
        [self.topic_type, self.topic_package] = self.extract_type_and_package(topic_package_type)
        self.dynamic_load(self.topic_package)
        self.subscriber = rospy.Subscriber(input_topic_name, eval(self.topic_type), self.sub_cb, queue_size=5)
        self.publisher = rospy.Publisher(output_topic_name, eval(self.topic_type), queue_size=1)
        rospy.loginfo("...topic retimer loaded")
        rospy.spin()

    def sub_cb(self, msg):
        retimed = deepcopy(msg)
        try:
            retimed.header.stamp = rospy.Time.now()
        except AttributeError:
            # rospy.logerr("Type does not have timestamps, you do not need to retime")
            # self.publisher.publish(msg)
            pass

        try:

        except AttributeError:
            pass

        self.publisher.publish(retimed)

    def extract_type_and_package(self, input_topic_type):
        topic_package = input_topic_type.split("/")[0]
        topic_type = input_topic_type.split("/")[1]
        return [topic_type, topic_package]

    def dynamic_load(self, package_name):
        rospy.loginfo("Dynamic loading messages from package: " + package_name)
        eval_str = "from " + package_name + ".msg import *"
        exec (eval_str, globals())


if __name__ == '__main__':
    rospy.init_node('topic_retimer')
    topic_package_type = rospy.get_param("~topic_package_type", "std_msgs/String")
    input_topic_name = rospy.get_param("~input_topic_name", "bad_timing/test")
    output_topic_name = rospy.get_param("~output_topic_name", "test")
    TopicRetimer(topic_package_type, input_topic_name, output_topic_name)