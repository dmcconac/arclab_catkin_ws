function [weights, fval, exitflag, output, lambda, grad, hessian] = complete_constrainted_minimization_experimental(initial_weights, lower_bound, upper_bound)
%% Start with the default options
options = optimset;
%% Modify options setting
options = optimset(options,'Display', 'off');
options = optimset(options,'FunValCheck', 'on');
% uncomment next line to plot weights
% options = optimset(options,'PlotFcns', { @optimplotx });
options = optimset(options,'Algorithm', 'interior-point');
options = optimset(options,'GradObj', 'off');
[weights, fval, exitflag, output, lambda, grad, hessian] = fmincon(@complete_cost_function_experimental, initial_weights, [], [], [], [], lower_bound, upper_bound, [], options);
