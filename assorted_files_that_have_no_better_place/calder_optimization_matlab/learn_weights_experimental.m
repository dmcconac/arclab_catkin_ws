function [] = learn_weights_experimental(base_filepath, number_of_demonstrations, number_of_samples, number_of_features, ignored_features, initial_weight_value, lower_bound_weight_value, upper_bound_weight_value, learned_weights_filepath, display_results)

%% Load the data

clearvars -global num_demonstrations num_samples num_features demonstrations_features samples_features

global num_demonstrations
global num_samples
global num_features
global demonstrations_features
global samples_features

num_demonstrations = number_of_demonstrations;
num_samples = number_of_samples;
num_features = number_of_features;

for demonstration_number = 1:number_of_demonstrations
    demonstration_features_filepath = strcat(base_filepath, sprintf('demonstration_%d_features_cumulative.csv', demonstration_number));
    demonstration_features = csvread(demonstration_features_filepath);
    demonstration_features(ignored_features) = [];
    demonstrations_features(:,demonstration_number) = demonstration_features;
    for sample_number = 1:number_of_samples
        sample_features_filepath = strcat(base_filepath, sprintf('demonstration_%d_sample_%d_features_cumulative.csv', demonstration_number, sample_number));
        sample_features = csvread(sample_features_filepath);
        sample_features(ignored_features) = [];
        % Method 1
%        samples_features(:,sample_number,demonstration_number) = sample_features;
        % Method 2
        sample_features = clean_sample_features(demonstration_features, sample_features, 1);
        samples_features(:,sample_number,demonstration_number) = sample_features;
        % Method 3
%        current_demonstration_features = demonstrations_features(:,demonstration_number);
%        sample_dominates_demonstration = check_if_sample_dominates(current_demonstration_features, sample_features);
%        if sample_dominates_demonstration
%            demonstrations_features(:,demonstration_number) = sample_features;
%            samples_features(:,sample_number,demonstration_number) = current_demonstration_features;
%        else
%            samples_features(:,sample_number,demonstration_number) = sample_features;
%        end
    end
end

%% Normalize the feature values between 0.0 and 1.0

% Get the maximum feature value in the demonstrations
max_demonstration_feature_value = max(max(demonstrations_features));
% Get the maximum feature value in the samples
max_sample_feature_value = max(max(max(samples_features)));
% Get the overall max feature value
max_feature_value = max([max_demonstration_feature_value, max_sample_feature_value]);
% Scale down all the feature values
inv_mag = 1.0 / max_feature_value;

demonstrations_features = demonstrations_features * inv_mag;
samples_features = samples_features * inv_mag;

%% Run the optimization

ignored_features_size = size(ignored_features);
number_of_ignored_features = max(ignored_features_size);

initial_weights = ones(1, number_of_features - number_of_ignored_features) * initial_weight_value;
lower_bound = ones(1, number_of_features - number_of_ignored_features) * lower_bound_weight_value;
upper_bound = ones(1, number_of_features - number_of_ignored_features) * upper_bound_weight_value;

fprintf('Learning weights...\n');
[weights, fval, exitflag, output, lambda, grad, hessian] = complete_constrainted_minimization_experimental(initial_weights, lower_bound, upper_bound);
fprintf('...learned\n');

% Add the ignored weights
used_features = [];
for idx = 1:num_features
    if ismember(idx, ignored_features)
        ;
    else
        used_features(end + 1) = idx;
    end
end

full_weights(used_features) = weights;
full_weights(ignored_features) = upper_bound_weight_value;
display(weights);
display(full_weights);

%% Save the learned features

csvwrite(learned_weights_filepath, full_weights);

if display_results
    actual_weights = [0.2, 0.2, 0.4, 0.4, 0.8, 0.8];
    display_learning_results(number_of_demonstrations, number_of_samples, demonstrations_features, samples_features, actual_weights, 'Actual');
    display_learning_results(number_of_demonstrations, number_of_samples, demonstrations_features, samples_features, full_weights, 'Learned');
end
exit();
