function loss = complete_cost_function_experimental(weights)

global num_demonstrations
global num_samples
global num_features
global demonstrations_features
global samples_features

% loss = 0.0;
% for d = 1:num_demonstrations
%     % Get the features for the current demonstration and its samples
%     current_demonstration_features = demonstrations_features(:,d);
%     current_samples_features = samples_features(:,:,d);
%     for sample_num = 1:num_samples
%         current_sample_features = current_samples_features(:,sample_num);
%         % Check if the sample has been NANed out
%         if all(isnan(current_sample_features))
%             ;
%         else
%             sample_val = weights * (current_sample_features - current_demonstration_features);
%             loss = loss - sample_val;
%         end
%     end
% end

loss = 0.0;
for d = 1:num_demonstrations
    % Get the features for the current demonstration and its samples
    current_demonstration_features = demonstrations_features(:,d);
    current_samples_features = samples_features(:,:,d);
    denominator = 0.0;
    for sample_num = 1:num_samples
        current_sample_features = current_samples_features(:,sample_num);
        % Check if the sample has been NANed out
        if all(isnan(current_sample_features))
            ;
        else
            denominator = denominator + (weights * current_sample_features);
        end
    end
    numerator = weights * current_demonstration_features;
    loss = loss + (numerator / denominator);
end